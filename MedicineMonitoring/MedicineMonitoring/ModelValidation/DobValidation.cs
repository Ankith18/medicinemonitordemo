﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.ModelValidation
{
    public class DobValidation : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                DateTime d = Convert.ToDateTime(value);
                if (d != null)
                {
                    if (d < DateTime.UtcNow)
                    {
                        return ValidationResult.Success;
                    }
                }
            }
            return new ValidationResult(ErrorMessage ?? "Please provide proper DOB");
        }

    }
   
    }

