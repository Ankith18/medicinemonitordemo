﻿using MedicineMonitoring.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedicineMonitoring.Controllers
{
    
    public class MedicineController : Controller
    {
        MedicineMonitoring.Methods.Methods methods = new MedicineMonitoring.Methods.Methods();
        // GET: Medicine
        MedicineDBContext _dbcontext = new MedicineDBContext();
        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult MedList()
        {
            if (_dbcontext.Medicines.ToList().Count == 0)
            {
                ViewBag.msg = 1;
                return View();
            }
            else
            {
                return View(_dbcontext.Medicines.ToList());
            }
        }

        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult AddMedicine()
        {
            ViewBag.BrachList = _dbcontext.Branches.Select(x => x.BranchName);
            ViewBag.BranchAddress = _dbcontext.Branches.Select(x => x.BranchAddress);
            Medicine medicine = new Medicine();
            return View(medicine);
        }
        [Authorize(Roles = "Admin,BranchAdmin")]
        [HttpPost]
        public ActionResult AddMedicine(Medicine medicine)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                var branch = _dbcontext.Branches.FirstOrDefault(x => x.BranchName == medicine.BranchAvaliable && x.BranchAddress == medicine.Address);

                if (branch != null)
                {
                    _dbcontext.Medicines.Add(medicine);
                    methods.Save(_dbcontext);
                    ViewBag.Message = "Medicine Details Added Succesfully";
                    return View();
                }
                else
                {
                    ViewBag.BrachList = _dbcontext.Branches.Select(x => x.BranchName);
                    ViewBag.BranchAddress = _dbcontext.Branches.Select(x => x.BranchAddress);
                   
                    ModelState.AddModelError("", "City is Mismatch with Branch");
                    return View();
                }
            }
        
        }

        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult Details(int? id)
        {
            if (id != null)
            {
                var medicine = _dbcontext.Medicines.Where(x => x.MedId == id).FirstOrDefault();
                return View(medicine);
            }

            return View();

        }

        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult Edit(int? id)
        {
            Medicine medicine = null;
            if (id != null)
            {
                ViewBag.BrachList = _dbcontext.Branches.Select(x => x.BranchName);
                ViewBag.BranchAddress = _dbcontext.Branches.Select(x => x.BranchAddress);
                medicine = _dbcontext.Medicines.Where(x => x.MedId == id).FirstOrDefault();

            }

            return View(medicine);

        }
        [Authorize(Roles = "Admin,BranchAdmin")]
        [HttpPost]
        public ActionResult Edit(Medicine medicine)
        {
            if (ModelState.IsValid)
            {

                _dbcontext.Entry(medicine).State = EntityState.Modified;
                methods.Save(_dbcontext);
                ViewBag.Message = "Medicine Details Updated Succesfully";
                return View();
            }
            return View();
        }
        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult StockList()
        {
            if (_dbcontext.Medicines.ToList().Count == 0)
            {
                ViewBag.msg = 1;
                return View();
            }
            return View(_dbcontext.Medicines.ToList());
        }
        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult EditStock(int? id)
        {
            Medicine medicine = null;
            if (id != null)
            {
                medicine = _dbcontext.Medicines.Where(x => x.MedId == id).FirstOrDefault();

            }

            return View(medicine);

        }
        [Authorize(Roles = "Admin,BranchAdmin")]
        [HttpPost]
        public ActionResult EditStock(Medicine medicine)
        {
            if (ModelState.IsValid)
            {
                _dbcontext.Entry(medicine).State = EntityState.Modified;
                methods.Save(_dbcontext);
                ViewBag.Message = "Medicine Stock Details Updated Succesfully";
                return View();
            }
            return View();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult BranchStock()
        {

            return View(_dbcontext.Branches.ToList());
        }
        [Authorize(Roles = "Admin")]
        public ActionResult ViewEachBranchStock(string id)
        {
            List<Medicine> stockList=new List<Medicine>();
            Branch branch = _dbcontext.Branches.FirstOrDefault(x => x.BranchId == id);
             stockList = _dbcontext.Medicines.Where(x => x.BranchAvaliable == branch.BranchName&&x.Address==branch.BranchAddress).ToList();
            var list = stockList.GroupBy(x => x.Disease).Select(Y => Y.FirstOrDefault()).ToList();
            
            return View(list);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult ViewEachBranchMedicine(int id)
        {
            Medicine medicine = _dbcontext.Medicines.FirstOrDefault(x => x.MedId == id);
            var stockList = _dbcontext.Medicines.Where(x => x.Disease==medicine.Disease&&x.BranchAvaliable==medicine.BranchAvaliable&&x.Address==medicine.Address).ToList();

            return View(stockList);
        }
    }
}