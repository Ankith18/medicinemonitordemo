﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.Models
{
    public class MedicineDBContext:DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Branch> Branches { get; set; }

        public DbSet<Medicine> Medicines { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        public System.Data.Entity.DbSet<MedicineMonitoring.ViewModels.UserBranch> UserBranches { get; set; }
    }
}